import tensorflow as tf
import tensorflowutilities.TensorflowOps as ops


def classify_deprec(image_batch: tf.Tensor, class_count: int, reuse: bool, scope: str="classifier") -> tf.Tensor:
	with tf.variable_scope(scope, values=[image_batch], reuse=reuse):
		conv_1 = ops.conv2d(image_batch, [5, 5], 32, stride=1, padding='VALID', scope="conv_1")
		pool_1 = ops.max_pool(conv_1, [2, 2], 2, padding='VALID', scope="max_pool_1")

		conv_2 = ops.conv2d(pool_1, [5, 5], 64, stride=1, padding='VALID', scope="conv_2")
		pool_2 = ops.max_pool(conv_2, [2, 2], 2, padding='VALID', scope="max_pool_2")

		flat_image = tf.contrib.layers.flatten(pool_2)

		lin_1 = ops.dense(flat_image, 512, activation=tf.nn.relu, scope="lin_1")
		return ops.dense(lin_1, class_count, activation=None, scope="lin_out")


def classify(image_batch: tf.Tensor, class_count: int, reuse: bool, trainable: bool=True, scope: str="classifier") -> tf.Tensor:
	with tf.variable_scope(scope, values=[image_batch], reuse=reuse):
		conv_1 = ops.conv2d(image_batch, [3, 3], 32, stride=2, padding='VALID', trainable=trainable, scope="conv_1")
		conv_2 = ops.conv2d(conv_1, [3, 3], 32, stride=1, padding='VALID', trainable=trainable, scope="conv_2")
		conv_3 = ops.conv2d(conv_2, [3, 3], 64, stride=1, padding='SAME', trainable=trainable, scope="conv_3")
		pool_1 = ops.max_pool(conv_3, [3, 3], 2, padding='VALID', scope="max_pool_1")

		conv_4 = ops.conv2d(pool_1, [1, 1], 80, stride=1, padding='VALID', trainable=trainable, scope="conv_4")
		conv_5 = ops.conv2d(conv_4, [3, 3], 192, stride=1, padding='VALID', trainable=trainable, scope="conv_5")
		pool_2 = ops.max_pool(conv_5, [3, 3], 2, padding='VALID', scope="max_pool_2")

		length = 192
		flat_image = tf.reshape(pool_2, [-1, length])

		lin_1 = ops.dense(flat_image, 64, activation=tf.nn.relu, trainable=trainable, scope="lin_1")
		return ops.dense(lin_1, class_count, activation=None, trainable=trainable, scope="lin_out")
