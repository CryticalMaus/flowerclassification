import tensorflow as tf
import cv2 as cv
import numpy as np
import MNIST.GAN as GAN
import MNIST.Dataset as Dataset
import MNIST.Classifier as Classifier
import MNIST.Generator as Generator

dataset = Dataset.Dataset("./MNIST_Settings.txt")

num_labels = dataset.label_class_count
batch_size = 10

#inputs
labels = tf.placeholder(dtype=tf.int32, shape=[None], name="correct_class")
image = tf.placeholder(dtype=tf.float32, shape=[None] + dataset.image_size + [1], name="image_example")
generator_noise = tf.placeholder(dtype=tf.float32, shape=[None, 100], name="noise")

optimizer, loss, accuracy =\
	GAN.build(image, num_labels, labels, generator_noise)
classifier = Classifier.classify(image, num_labels + 1, True, False)
generator = Generator.generate(tf.one_hot(labels, num_labels+1), generator_noise, True, False)

config = tf.ConfigProto(
	device_count={'GPU': 1, 'CPU': 8}
)
with tf.Session(config=config) as session:
	session.run(tf.global_variables_initializer())
	merged_summary = tf.summary.merge_all()
	writer = tf.summary.FileWriter("./graphs/MNIST")
	writer.add_graph(session.graph)


	print("Beginning training")
	for i in range(0, dataset.train_length, batch_size):
		train_images, train_labels = dataset.get_train_batch(i, batch_size)

		image_count = len(train_labels)
		noise = np.reshape(np.random.uniform(-1, 1, 100 * image_count), [image_count, 100])

		_, summary, gan_loss, gan_accuracy = session.run([optimizer, merged_summary, loss, accuracy],
								 feed_dict={labels: train_labels,
											image: train_images,
											generator_noise: noise})
		writer.add_summary(summary)
		print("loss: {:5.5}, accuracy: {:1.4}, training progress: {:1.4}%".format(
			gan_loss, gan_accuracy, float(i) / dataset.train_length))
		if i % (batch_size * 5) == 0:
			image = generator.eval(feed_dict={labels: train_labels,
											generator_noise: noise})
			cv.imshow("image", image[0])
			cv.waitKey(1)

	test_images, test_labels = dataset.get_test_batch(0, dataset.test_length)
	logits = classifier.eval(feed_dict={image: test_images})
	test_accuracy = 0
	for logit, label in zip(logits, test_labels):
		if label == np.argmax(logit):
			test_accuracy += 1
	test_accuracy /= len(test_labels)
	print("Test Accuracy: {:1.4}".format(test_accuracy))

cv.waitKey()