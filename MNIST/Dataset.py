import cv2 as cv
import struct
import numpy as np


def _load_MNIST(path_img, path_lbl):
    with open(path_lbl, 'rb') as label_file:
        magic, size = struct.unpack(">II", label_file.read(8))
        if magic != 2049:
            raise ValueError('Magic number mismatch, expected 2049,'
                             'got {}'.format(magic))

        labels = np.fromstring(label_file.read(), dtype=np.uint8)

    with open(path_img, 'rb') as file:
        magic, size, rows, cols = struct.unpack(">IIII", file.read(16))
        if magic != 2051:
            raise ValueError('Magic number mismatch, expected 2051,'
                             'got {}'.format(magic))

        image_data = np.fromstring(file.read(), dtype=np.uint8)
        images = image_data.reshape(size, 28, 28, 1).astype(np.float32) * (1.0 / 255)

    return images, labels


class Dataset:
	def __init__(self, settings):
		with open(settings, 'r') as file:
			self._mnist_location = file.readline().strip()[len("MNIST Training Images="):]
			self._testing_size = file.readline().strip()[len("Testing Size="):]

			self.train_images, self.train_data_labels = _load_MNIST(
				self._mnist_location + "train-images.idx3-ubyte", self._mnist_location + "train-labels.idx1-ubyte")
			self.test_images, self.test_data_labels = _load_MNIST(
				self._mnist_location + "t10k-images.idx3-ubyte", self._mnist_location + "t10k-labels.idx1-ubyte")
			self.train_length = len(self.train_data_labels)
			self.test_length = len(self.test_data_labels)
			self.label_class_count = 10
			self.image_size = [28, 28]

	def get_train_batch(self, index: int, batch_size: int) -> [np.ndarray, np.ndarray]:
		assert (index >= 0)

		batch = list()
		end = index + batch_size
		if end >  self.train_length:
			end = self.train_length

		return self.train_images[index: end], self.train_data_labels[index: end]

	def get_test_batch(self, index: int, batch_size: int) -> [np.ndarray, np.ndarray]:
		assert (index >= 0)

		batch = list()
		end = index + batch_size
		if end > self.test_length:
			end = self.test_length

		return self.test_images[index: end], self.test_data_labels[index: end]
