import tensorflow as tf
import cv2 as cv
import numpy as np
import Flowers.Dataset as Dataset
import Flowers.Dataset as Dataset
import Flowers.GAN


#load settings
# settings = Dataset.Settings("./settings.txt")

#construct training and test sets
#dataset = Dataset.Dataset(settings)
dataset = Dataset.Dataset("./MNIST_Settings.txt")

num_labels = dataset.label_class_count
batch_size = 5

#inputs
correct_flower_class = tf.placeholder(dtype=tf.int32, shape=[None], name="correct_class")
flower_image = tf.placeholder(dtype=tf.float32, shape=[None] + dataset.image_size + [1], name="image_example")
generator_noise = tf.placeholder(dtype=tf.float32, shape=[None, 100], name="noise")

classifier_optimizer, generator_optimizer, generate_flower, classify_loss, generator_loss, accuracy =\
	GAN.build(flower_image, num_labels, correct_flower_class, generator_noise)

config = tf.ConfigProto(
	device_count={'GPU': 1, 'CPU': 8}
)
with tf.Session(config=config) as session:
	session.run(tf.global_variables_initializer())
	merged_summary = tf.summary.merge_all()
	writer = tf.summary.FileWriter("./graphs/1")
	writer.add_graph(session.graph)


	print("Beginning training")
	for i in range(0, dataset.train_length, batch_size):
		train_images, train_labels = dataset.get_train_batch(i, batch_size)

		cv.imshow("Flower", train_images[0])
		cv.waitKey(1)

		image_count = len(train_labels)
		noise = np.reshape(np.random.uniform(-1, 1, 100 * image_count), [image_count, 100])

		_, summary, acc = session.run([classifier_optimizer, merged_summary, accuracy],
								 feed_dict={correct_flower_class: train_labels,
											flower_image: train_images,
											generator_noise: noise})
		writer.add_summary(summary)
		print(str(acc) + ", " + str(float(i) / dataset.train_length) + "%")
		if i % (batch_size * 5) == 0:
			continue
			flower = generate_flower.eval(feed_dict={correct_flower_class: train_labels,
											flower_image: train_images,
											generator_noise: noise})
			cv.imshow("Flower", flower[0])
			cv.waitKey(1)
			print("flower")

cv.waitKey()