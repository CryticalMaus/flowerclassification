import Flowers.Classifier as Classifier
import Flowers.Generator as Generator
import tensorflow as tf


def build(flower_image: tf.Tensor, num_labels: int, correct_flower_class: tf.Tensor, generator_noise: tf.Tensor):
	'''
	generated_data = Generator.build_model(correct_flower_class,
										   num_labels + 1,
										   generator_noise, False,
										   scope="generator")
	generated_class = tf.ones_like(correct_flower_class) * (num_labels - 1)
	correct_classes = tf.concat([correct_flower_class, generated_class], 0)
	classify_flower = Classifier.classify(tf.concat([flower_image, generated_data], 0),
										  num_labels + 1, False, scope="classifier")
	'''
	one_hot_classes = tf.one_hot(correct_flower_class, num_labels + 1)
	classify_flower = Classifier.classify(flower_image, num_labels + 1, False, scope="classifier")

	with tf.name_scope("classifier_loss", values=[one_hot_classes, classify_flower]):
		classify_loss = tf.losses.softmax_cross_entropy(onehot_labels=one_hot_classes, logits=classify_flower)
		#classify_loss = tf.clip_by_value(classify_loss, -10, 10)
	tf.summary.scalar("classifier_loss", classify_loss)

	with tf.name_scope("accuracy", values=[one_hot_classes, classify_flower]):
		correct_prediction = tf.equal(tf.argmax(one_hot_classes, 1), tf.argmax(classify_flower, 1))
		accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
		accuracy = tf.Print(accuracy, [tf.argmax(one_hot_classes, 1), tf.argmax(classify_flower, 1)],
							"actual/prediction", summarize = 10)
	tf.summary.scalar("accuracy", accuracy)


	'''
	generate_flower = Generator.build_model(correct_flower_class, num_labels + 1, generator_noise, True,
											scope="generator")
	classify_flower = Classifier.classify(generate_flower, num_labels + 1, True, scope="classifier")
	generator_loss = tf.reduce_mean(
		tf.nn.sparse_softmax_cross_entropy_with_logits(labels=correct_flower_class, logits=classify_flower))

	tf.summary.scalar("generator_loss", generator_loss)
	'''

	t_vars = tf.trainable_variables()
	generator_trainables = [var for var in t_vars if var.name.startswith('generator')]
	classifier_trainables = [var for var in t_vars if var.name.startswith('classifier')]

	classifier_optimizer = tf.train.AdamOptimizer(0.00002).minimize(classify_loss)
	#generator_optimizer = tf.train.AdamOptimizer(0.5).minimize(-generator_loss, var_list=generator_trainables)

	generate_flower = tf.constant(0)
	generator_loss = tf.constant(0)
	generator_optimizer = tf.constant(0)

	return classifier_optimizer, generator_optimizer, generate_flower, classify_loss, generator_loss, accuracy

