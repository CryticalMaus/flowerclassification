import tensorflow as tf
import tensorflowutilities.TensorflowOps as ops


def build_model(image_class_index: tf.Tensor, class_count: int, seed: tf.Tensor, reuse: bool, scope: str="generator"):
	with tf.variable_scope(scope, values=[image_class_index, seed], reuse=reuse):
		one_hot_class = tf.one_hot(image_class_index, class_count)
		merged = tf.concat([one_hot_class, seed], 1)
		context = ops.linear(merged, 100, scope="lin_1")
		context = ops.linear(context, 4*4*1024, scope="lin_2")
		seed_image = tf.reshape(context, [-1, 4, 4, 1024])

		iconv_1 = tf.nn.tanh(ops.inv_conv2d(seed_image, [8, 8, 512], [5, 5], scope="iconv_1"))
		iconv_2 = tf.nn.tanh(ops.inv_conv2d(iconv_1, [16, 16, 256], [5, 5], scope="iconv_2"))
		iconv_3 = tf.nn.tanh(ops.inv_conv2d(iconv_2, [32, 32, 128], [5, 5], scope="iconv_3"))
		iconv_4 = tf.nn.tanh(ops.inv_conv2d(iconv_3, [64, 64, 64], [5, 5], scope="iconv_4"))
		iconv_5 = tf.nn.sigmoid(ops.inv_conv2d(iconv_4, [128, 128, 3], [5, 5], scope="iconv_5"))
		return iconv_5

