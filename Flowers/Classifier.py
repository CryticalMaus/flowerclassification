import tensorflow as tf
import tensorflowutilities.TensorflowOps as ops


def _layer(input: tf.Tensor, out_size: int, name:str="layer"):
	with tf.variable_scope(name, values=[input]):
		reduced_image_x2 = tf.layers.conv2d(
			inputs=input,
			kernel_initializer=tf.random_uniform_initializer(),
			filters=out_size,
			kernel_size=[5, 5],
			padding="same",
			activation=tf.nn.relu)
		return tf.layers.max_pooling2d(inputs=reduced_image_x2, pool_size=[2, 2], strides=2)


def classify(image_batch: tf.Tensor, class_count: int, reuse: bool, data_format: str="NHWC", scope: str="classifier") -> tf.Tensor:
	with tf.variable_scope(scope, values=[image_batch], reuse=reuse):
		'''
		with tf.name_scope("layer_1", values=[image_batch]):
			conv_1 = tf.nn.relu(ops.convolve2d(image_batch, 64, filter_shape=[7, 7], strides=[2, 2],
									data_format=data_format, scope="conv_1"))
		with tf.name_scope("layer_2", values=[conv_1]):
			conv_2 = tf.nn.relu(ops.convolve2d(conv_1, 128, filter_shape=[7, 7], strides=[2, 2],
											   data_format=data_format, scope="conv_2"))
		with tf.name_scope("layer_3", values=[conv_2]):
			conv_3 = tf.nn.relu(ops.convolve2d(conv_2, 256, filter_shape=[7, 7], strides=[2, 2],
											   data_format=data_format, scope="conv_3"))
		with tf.name_scope("layer_4", values=[conv_3]):
			conv_4 = tf.nn.relu(ops.convolve2d(conv_3, 512, filter_shape=[7, 7], strides=[2, 2],
											   data_format=data_format, scope="conv_4"))
		with tf.name_scope("layer_5", values=[conv_4]):
			conv_5 = tf.nn.relu(ops.convolve2d(conv_4, 1024, filter_shape=[7, 7], strides=[2, 2],
											   data_format=data_format, scope="conv_5"))
		'''
		conv_1 = ops.conv2d(image_batch, 32, [3, 3], stride=2, padding='VALID', scope="conv_1")
		conv_2 = ops.conv2d(conv_1, 32, [3, 3], stride=1, padding='VALID', scope="conv_2")
		conv_3 = ops.conv2d(conv_2, 64, [3, 3], stride=1, padding='SAME', scope="conv_3")
		pool_1 = ops.max_pool(conv_3, [3, 3], 2, padding='VALID', scope="max_pool_1")

		conv_4 = ops.conv2d(pool_1, 80, [1, 1], stride=1, padding='VALID', scope="conv_4")
		conv_5 = ops.conv2d(conv_4, 192, [3, 3], stride=1, padding='VALID', scope="conv_5")
		pool_2 = ops.max_pool(conv_5, [3, 3], 2, padding='VALID', scope="max_pool_2")

		flat_image = tf.contrib.layers.flatten(pool_2)

		lin_1 = tf.layers.dense(inputs=flat_image, units=8, activation=tf.nn.relu, name="lin_1")
		return tf.layers.dense(inputs=lin_1, units=class_count, activation=tf.nn.sigmoid, name="lin_4")
