import tensorflow as tf
import MNIST.Classifier as Classifier
import MNIST.Generator as Generator


def build(real_image: tf.Tensor, num_labels: int, correct_class: tf.Tensor, noise: tf.Tensor):
	num_labels += 1
	real_labels = tf.one_hot(correct_class, num_labels, name="labels")

	with tf.name_scope("classifier"):
		fake_image = Generator.generate(real_labels, noise, False, trainable=False, scope="generator")
		logits = Classifier.classify(
			tf.concat([real_image, fake_image], 0),
			num_labels, False, trainable=True, scope="classifier")
		fake_labels = tf.one_hot(tf.ones_like(correct_class) * (num_labels - 1), num_labels, name="fake_labels")
		labels = tf.concat([real_labels, fake_labels], 0)
		classify_loss = tf.losses.softmax_cross_entropy(onehot_labels=labels, logits=logits, scope="loss")
	tf.summary.scalar("classifier_loss", classify_loss)


	with tf.name_scope("accuracy", values=[labels, logits]):
		correct_prediction = tf.equal(tf.argmax(labels, 1), tf.argmax(logits, 1))
		accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
		accuracy = tf.Print(accuracy, [tf.argmax(labels, 1), tf.argmax(logits, 1)], summarize=5)
	tf.summary.scalar("accuracy", accuracy)


	with tf.name_scope("generator"):
		labels = tf.concat(
			[
				real_labels,
				tf.one_hot(tf.random_uniform(tf.shape(correct_class), 0, num_labels - 1, tf.int32), num_labels)
			],
			0,
			name="fake_labels")
		noise = tf.concat([tf.random_uniform(tf.shape(noise)), noise], 0, name="noise")
		fake_image = Generator.generate(labels, noise, True, trainable=True, scope="generator")
		logits = Classifier.classify(fake_image, num_labels, True, trainable=False, scope="classifier")
		generator_loss = tf.losses.softmax_cross_entropy(onehot_labels=labels, logits=logits, scope="loss")


	"""
	t_vars = tf.trainable_variables()
	classifier_trainables = [var for var in t_vars if var.name.startswith('classifier')]
	"""

	loss = classify_loss + generator_loss
	optimizer = tf.train.GradientDescentOptimizer(0.1).minimize(loss)

	return optimizer, loss, accuracy

