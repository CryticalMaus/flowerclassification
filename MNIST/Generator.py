import tensorflow as tf
import tensorflowutilities.TensorflowOps as ops


def generate(onehot_class: tf.Tensor, seed: tf.Tensor, reuse: bool, trainable: bool=True, scope: str="generator"):
	with tf.variable_scope(scope, values=[onehot_class, seed], reuse=reuse):
		merged = tf.concat([onehot_class, seed], 1)
		context = ops.dense(merged, 256, trainable=trainable, scope="lin_1")
		context = ops.dense(context, 7*7*196, trainable=trainable, scope="lin_2")
		seed_image = tf.reshape(context, [-1, 7, 7, 196])

		iconv_2 = tf.nn.tanh(ops.inv_conv2d(seed_image, [14, 14], 32, [3, 3],
											stride=2, trainable=trainable, scope="iconv_2"))
		iconv_3 = tf.nn.sigmoid(ops.inv_conv2d(iconv_2, [28, 28], 1, [3, 3],
											   stride=2, trainable=trainable, scope="iconv_3"))
		return iconv_3

